# Docker Compose, Flask, Vue.js, and MongoDB

This is a variant of the [Flask and Docker Compose with
CI](https://gitlab.com/TrendDotFarm/docker-tutorial) project. But instead of
using MySQL, it uses MongoDB. And as a front-end it uses Vue.js.

## GitLab CI

The [.gitlab-ci.yml](.gitlab-ci.yml) file can be used in GitLab to build, test,
and deploy the code. For more information, read the [Docker Compose Integration
to GitLab CI](GitLab-CI.md) guide.
