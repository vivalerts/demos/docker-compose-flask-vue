#!/usr/bin/env python
'''A simple Flask app application'''

import json

from flask import Flask
from pymongo import MongoClient

app = Flask(__name__)


def favorite_colors():
    '''Fetch rows from database'''
    client = MongoClient('db')
    collection = client.knights.favorite_colors
    cursor = collection.find({}, {'_id': 0, 'color': 1})
    return list(cursor)


@app.route('/api/')
def index():
    '''The main page of the site'''
    return json.dumps({'favorite_colors': favorite_colors()})


if __name__ == '__main__':
    app.run(host='0.0.0.0')
