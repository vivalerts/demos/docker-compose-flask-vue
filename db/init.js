db = db.getSiblingDB('knights');
db.favorite_colors.insertMany([
  {
    name: 'Lancelot',
    color: 'blue'
  },
  {
    name: 'Galahad',
    color: 'yellow'
  }
]);
